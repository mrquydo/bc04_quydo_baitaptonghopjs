function tinhGoc() {
  let hour = document.getElementById("txt-hour").value * 1;
  let minute = document.getElementById("txt-minute").value * 1;

  let hourDegree = 0.5 * (hour * 60 + minute);
  let minuteDegree = 6 * minute;

  let result = Math.abs(minuteDegree - hourDegree);

  document.getElementById("result").innerText = "Góc lệch là: " + result;
}
