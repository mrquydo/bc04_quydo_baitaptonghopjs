var numArr = [];

function themSo() {
  var number = document.getElementById("txt-number").value * 1;
  //   Reset ô input sau khi user nhập
  document.getElementById("txt-number").value = "";

  var addedNum = document.getElementById("array-result");

  //   Thêm phần tử vào mảng
  numArr.push(number);

  addedNum.innerHTML = "Mảng hiện tại: " + numArr;
}

function isPrime(n) {
  let flag = 1;
  //flag = 0 => không phải số nguyên tố
  //flag = 1 => số nguyên tố

  if (n < 2)
    return (flag = 0); /*Số nhỏ hơn 2 không phải số nguyên tố => trả về 0*/

  /*Sử dụng vòng lặp while để kiểm tra có tồn tại ước số nào khác không*/
  let i = 2;
  while (i < n) {
    if (n % i == 0) {
      flag = 0;
      break; /*Chỉ cần tìm thấy 1 ước số là đủ và thoát vòng lặp*/
    }
    i++;
  }

  return flag;
}

function showPrime() {
  let primeResult = [];
  for (let i = 0; i < numArr.length; i++) {
    if (isPrime(numArr[i]) == 1) {
      primeResult.push(numArr[i]);
    }
  }
  document.getElementById("prime-result").innerText =
    "Số nguyên tố trong mảng là" + primeResult;
}
